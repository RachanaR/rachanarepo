import { applyMiddleware, compose, createStore } from 'redux';
import reducer from './ReducerIndex';
import thunk from 'redux-thunk';

const AppStore=createStore(reducer,compose(applyMiddleware(thunk)));

export default AppStore;

