class columnDefs{
    static getPostsColumnDefs(){
        return[
            {
                headerName: 'ID',
                field: 'id',
                width: 150,
                 cellRenderer: 'agGroupCellRenderer',
            },
            {
                headerName: 'User Id',
                field: 'userId',
                width: 150,
            },
            {
                headerName: 'Title',
                field: 'title',
                 width: 350,
                 tooltipField:'title'
            },
            {
                 headerName: 'Body',
                 field: 'body',
                 width: 500,
                 tooltipField:'body'
            },
        ]
    }
    static getCommentsForPost(){
      return[
                {
                    headerName: 'Post ID',
                    field: 'postId',
                    width: 100,
                },
                {
                    headerName: 'Id',
                    field: 'id',
                    width: 150,
                },
                {
                    headerName: 'Name',
                    field: 'name',
                     width: 150,
                     tooltipField:'name'
                },
                {
                     headerName: 'Email',
                     field: 'email',
                     width: 200,
                     tooltipField:'email'
                },
                {
                      headerName: 'Body',
                       field: 'body',
                      width: 300,
                      tooltipField:'body'
                },
            ]
    }
}
export default columnDefs;