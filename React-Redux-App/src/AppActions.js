
export const getPostsAction=(data)=>({
    type:'POSTS',
    payload:data
})

export const resetActionTypeAction=()=>({
    type:'RESET',
    payload:''
})
export const getPosts=()=>{
    return dispatch=>{
        return fetch('https://jsonplaceholder.typicode.com/posts').then(resp=>{
            resp.json().then(data=>{
                if(data){
                    dispatch(getPostsAction(data))
                }
            });
        });
    }
}
export const resetActionType=()=>{
    return dispatch=>{
        dispatch(resetActionTypeAction())
    }
}


