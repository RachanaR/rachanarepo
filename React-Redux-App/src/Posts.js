import React, { Component } from 'react';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import * as AppActions from './AppActions';
import {AgGridReact} from 'ag-grid-react';
import 'ag-grid-enterprise';
import ColumnDefs from './ColumnDefs';
import 'ag-grid-community/dist/styles/ag-grid.css';
import 'ag-grid-community/dist/styles/ag-theme-material.css';
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap/dist/css/bootstrap.min.css'

class Posts extends Component {
    constructor(props){
        super(props);
        this.state={
        columnDefs: ColumnDefs.getPostsColumnDefs(),
        rowData:[],
        commentsRenderer:{
            detailGridOptions: {
            rowHeight:32,
            columnDefs:ColumnDefs.getCommentsForPost(),
            onFirstDataRendered : (params) => {
                params.api.sizeColumnsToFit();
              }
            },
            getDetailRowData: function(params) {
                setTimeout(() => {
                    fetch('https://jsonplaceholder.typicode.com/posts/'+ params.data.id +'/comments').then(resp=>{
                                        resp.json().then(data=>{
                                            if(data){
                                               params.successCallback(data)
                                            }
                                        });
                                    })
                },1000)
            }
        }
        };
    }
    componentDidMount(){
        this.props.actions.getPosts();
    }
    componentDidUpdate(){
         console.log('hiiii')
        if(this.props.appData && this.props.appData.actionType){
            switch(this.props.appData.actionType){
                case 'POSTS':
                this.updatePostData(this.props.appData.posts);
                break;
            }
            this.props.actions.resetActionType();
        }
    }
    updatePostData=(posts)=>{
       this.setState({rowData:posts});
    }
    onGridReady=(params)=>{
        this.gridApi=params.api;
        this.columnGridApi=params.columnApi;
        this.gridApi.setDomLayout("autoHeight")
    }
    render() {
        return (
        <>
        <div className="row" style={{paddingLeft:'34%'}}><h2>POSTS AND COMMENTS</h2></div>
        <div className="row">
        <div className='col-md-1'></div>
            <div className='col-md-10' style={{border:'1px solid black'}} >
                <div id="myGrid"  className="ag-theme-material">
                    <AgGridReact
                        onGridReady={this.onGridReady}
                        columnDefs={this.state.columnDefs}
                        rowData={this.state.rowData}
                         masterDetail= {true}
                        detailCellRendererParams= {this.state.commentsRenderer}
                        pagination
                        rowHeight={32}
                        headerHeight={50}
                        paginationPageSize={10}
                    />
                </div>
            </div>
        <div className='col-md-1'></div>
        </div>
         </>
        );
    }
}
const mapStateToProps=(state,props)=>{
    return({
        appData:state.AppReducer.appData,
        refreshIndex:state.AppReducer.refreshIndex
    })
}
const mapDispatchToProps=(dispatch)=>{
     return({
        actions:bindActionCreators(AppActions,dispatch)
     })
}
export default connect(mapStateToProps,mapDispatchToProps)(Posts);